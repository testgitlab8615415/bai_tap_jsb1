
/**
 * input: Nhập vào số ngày làm 
 * b1:tạo biến: luongMotNgay, soNgayLam,result
 * b2: gán giá trị cho luongMotNgay và soNgayLam
 * b3: tính tiền lương.
 * output: là tiền lương nhận được sau những ngày làm .

*/

function tinhTienLuong(){
var luongMotNgay=100000;
var soNgayLam=(document.getElementById("so_ngay_lam")).value;
 document.getElementById("result").innerHTML=(luongMotNgay*soNgayLam).toLocaleString()+ " VND";
}


/**
 * input: nhập 5 số vào các input 
 * b1:tạo biến num1->num5
 * b2:gán giá trị cho từng biến(num1->num5)
 * b3:tính giá trị trung bình
 * output: giá trị trung bình của 5 số
 */

function tinhGiaTriTrungBinh(){
    var num1=document.getElementById("so1").value*1;
    var num2=document.getElementById("so2").value*1;
    var num3=document.getElementById("so3").value*1;
    var num4=document.getElementById("so4").value*1;
    var num5=document.getElementById("so5").value*1;
document.getElementById("trung_binh").innerHTML=(num1+num2+num3+num4+num5)/5;
}

/**
 * input: nhập vào số usd
 * b1:tạo biến giaUSD,tienUSD
 * b2:gán giá trị cho hai biến trên
 * b3: tính số tiền sau quy đổi.
 * output: số tiền vnđ sau khi quy đổi 
 */
function quyDoiTien(){
    var giaUSD=23500;
    var tienUSD=document.getElementById("tien_usd").value;
   document.getElementById("tien_quy_doi").innerHTML=(giaUSD*tienUSD).toLocaleString()+ " VND";
}

/**
 * input: nhập vào lần lượt chiều dài và chiều rộng của hình chữ nhật 
 * b1: tạo biến height và width
 * b2: gán giá trị cho hai biến
 * b3: tính diện tích và chu vi
 * output: diện tích và chu vi của hình chữ nhật 
 */
function dienTichVaChuViHCN(){
    var height=document.getElementById("chieu_dai").value*1;
    var width=document.getElementById("chieu_rong").value*1;
    document.getElementById("dien_tich_va_chu_vi").innerHTML=`diện tích của hình chữ nhật là: ${height*width}, diện tích của hình chữ nhật là: ${(height+width)*2}`
}
/**
 * input: nhập vào một số có hai chữ số
 * b1: tạo biến soHangChuc, soHangDonVi
 * b2: gán giá trị cho hai biến
 * b3: tính tổng các kí số
 * output: tổng các ký số 
 */
function tongKySo(){
   var soHangChuc= document.getElementById("so_co_hai_chu_so").value/10;
   var soHangDonVi=document.getElementById("so_co_hai_chu_so").value%10;
   document.getElementById("ket_qua").innerHTML=Math.floor(soHangChuc+soHangDonVi);
}